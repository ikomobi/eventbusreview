package com.ikomobi.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import de.greenrobot.event.EventBus;

/**
 * Créé vincentmasselis on 02/09/2014.
 */
public class ParcelableBus extends EventBus implements Parcelable {

    private static Map<String, ParcelableBus> eventBusMap = new HashMap<String, ParcelableBus>();

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        String identifier = UUID.randomUUID().toString();
        eventBusMap.put(identifier, this);
        dest.writeString(identifier);
    }

    public static final Creator<ParcelableBus> CREATOR = new Creator<ParcelableBus>() {
        public ParcelableBus createFromParcel(Parcel in) {
            return eventBusMap.remove(in.readString());
        }

        public ParcelableBus[] newArray(int size) {
            return new ParcelableBus[size];
        }
    };
}
