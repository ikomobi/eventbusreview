package com.ikomobi.app.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.ikomobi.app.R;
import com.ikomobi.app.model.ParcelableBus;

import roboguice.fragment.RoboDialogFragment;
import roboguice.inject.InjectView;

/**
 * Créé vincentmasselis on 02/09/2014.
 */
public class MainChildFragment extends RoboDialogFragment {

    private static final String EVENT_BUS_PARAMETER = "EVENT_BUS_PARAMETER";
    private ParcelableBus mBus;

    @InjectView(R.id.main_btn_child_throw_event)
    Button mButton;
    @InjectView(R.id.main_et_child_throw_event)
    EditText mEditText;

    public static MainChildFragment newInstance(ParcelableBus bus) {
        MainChildFragment fragment = new MainChildFragment();
        Bundle bundle = new Bundle();
        // Le bus est envoyé à l'instance du Fragment dans un bundle d'arguments
        // En cas de changement d'orientation, le bundle est renvoyé automatiquement au fragment.
        // Cette approche permet d'éviter de devoir gérer l'instance du bus parent dans la méthode onSaveInstanceState
        bundle.putParcelable(EVENT_BUS_PARAMETER, bus);
        fragment.setArguments(bundle);
        return fragment;
    }

    public MainChildFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Récupération du bus donné par le parent
        mBus = getArguments().getParcelable(EVENT_BUS_PARAMETER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_child_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Quand on clique sur "Throw event", on récupère la valeur de l'EditText et on la stocke dans l'évènement MainFragmentButtonEvent
                ChildFragmentButtonEvent event = new ChildFragmentButtonEvent(mEditText.getText().toString());

                // On poste l'évènement précédement créé sur le bus donné par le parent.
                mBus.post(event);
            }
        });
    }

    /**
     * @see com.ikomobi.app.ui.MainFragment.MainFragmentButtonEvent pour les conventions et une description
     */
    public class ChildFragmentButtonEvent {
        public final String value;

        public ChildFragmentButtonEvent(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
