package com.ikomobi.app.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ikomobi.app.R;
import com.ikomobi.app.model.ParcelableBus;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * Créé vincentmasselis on 01/09/2014.
 */
public class MainFragment extends RoboFragment {

    private static final String EVENT_BUS_PARAMETER = "EVENT_BUS_PARAMETER";
    private static final String EVENT_CHILD_BUS_STATE = "EVENT_CHILD_BUS_STATE";
    private ParcelableBus mParentBus;
    private ParcelableBus mChildBus;

    @InjectView(R.id.main_fragment_tv)
    TextView mTextView;
    @InjectView(R.id.main_fragment_btn)
    Button mButton;
    @InjectView(R.id.main_fragment_et)
    EditText mEditText;

    public static MainFragment newInstance(ParcelableBus bus) {
        MainFragment fragment = new MainFragment();
        Bundle bundle = new Bundle();
        // Le bus est envoyé à l'instance du Fragment dans un bundle d'arguments
        // En cas de changement d'orientation, le bundle est renvoyé automatiquement au fragment.
        // Cette approche permet d'éviter de devoir gérer l'instance du bus parent dans la méthode onSaveInstanceState
        bundle.putParcelable(EVENT_BUS_PARAMETER, bus);
        fragment.setArguments(bundle);
        return fragment;
    }

    public MainFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Récupération du bus donné par le parent
        mParentBus = getArguments().getParcelable(EVENT_BUS_PARAMETER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        if (savedInstanceState == null) {
            //Création du bus qui va être transmi à MainChildFragment
            mChildBus = new ParcelableBus();

            // Création du fragment MainChildFragment est ajout de celui-ci dans un FrameLayout
            // Le Fragment est créé avec en paramètre d'entré le bus précédement créé
            // Il s'agit du même principe que ce qui est mis en place entre MainActivity et MainFragment
            getChildFragmentManager().beginTransaction().add(R.id.main_fl_child_container, MainChildFragment.newInstance(mChildBus)).commit();
        }
        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Quand on clique sur "Throw event", on récupère la valeur de l'EditText et on la stocke dans l'évènement MainFragmentButtonEvent
                MainFragmentButtonEvent event = new MainFragmentButtonEvent(mEditText.getText().toString());

                // On poste l'évènement précédement créé sur le bus donné par le parent.
                mParentBus.post(event);
            }
        });
    }

    @Override
    public void onViewStateRestored(Bundle inState) {
        super.onViewStateRestored(inState);
        if (inState != null) {
            // Récupération du bus donné à MainChildFragment à partir de l'instance précédente du fragment : http://developer.android.com/guide/topics/resources/runtime-changes.html
            mChildBus = inState.getParcelable(EVENT_CHILD_BUS_STATE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // A chaque fois que l'écran est en foreground on écoute les évènements, c'est un choix arbitraire, on pourrait très bien utiliser onStart() et onStop()
        mChildBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Dès que l'écran passe en background, on écoute plus les évènements
        mChildBus.unregister(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // L'instance courante du fragment va disparaître, par conséquence on sauvegarde le bus : http://developer.android.com/guide/topics/resources/runtime-changes.html
        outState.putParcelable(EVENT_CHILD_BUS_STATE, mChildBus);
    }

    /**
     * Appelé quand MainChildFragment poste un évènement du type ChildFragmentButtonEvent sur le bus qu'on lui a donné en paramètre
     *
     * @param event Evènement envoyé par MainChildFragment
     */
    @SuppressWarnings("unused")
    public void onEvent(MainChildFragment.ChildFragmentButtonEvent event) {
        mTextView.setText("MainFragment : " + event.getValue());
    }

    /**
     * Cette classe représente un évènement spécifique, il ne faut jamais envoyer directement la donnée à travers le bus
     * car si un autre évènement poste un donnée du même type que la donnée actuelle, la même méthode onEvent() sera appelée sur le parent
     * et le parent ne saura pas identifier exactement de quel evènement il s'agit.
     * <p/>
     * Par convention, un évènement est un objet immutable, chacune des données qu'il contient ne doit pas évoluer une fois que l'objet est créé
     */
    public class MainFragmentButtonEvent {

        // Pour rentre la donnée immutable, les plus simple reste l'utilisation du final
        // Il vous utilisez une liste, l'API Guava propose pas mal d'outils pour rentre des Collections immutable
        public final String value;

        public MainFragmentButtonEvent(String value) {
            this.value = value;
        }

        // Pas de setter pour un objet immutable, juste le getter
        public String getValue() {
            return value;
        }
    }
}
