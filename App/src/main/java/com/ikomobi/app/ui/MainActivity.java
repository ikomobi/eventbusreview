package com.ikomobi.app.ui;

import android.os.Bundle;
import android.widget.TextView;

import com.ikomobi.app.R;
import com.ikomobi.app.model.ParcelableBus;

import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.InjectView;


public class MainActivity extends RoboActionBarActivity {

    private static final String EVENT_BUS_STATE = "EVENT_BUS_STATE";
    private ParcelableBus mBus;

    @InjectView(R.id.main_activity_tv)
    TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            //Création du bus qui va être transmi à MainFragment
            mBus = new ParcelableBus();


            // Création du fragment MainFragment est ajout de celui-ci dans un FrameLayout
            // Le Fragment est créé avec en paramètre d'entré le bus précédement créé
            getSupportFragmentManager().beginTransaction().add(R.id.fl_fragment_container, MainFragment.newInstance(mBus)).commit();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle inState) {
        super.onRestoreInstanceState(inState);
        // On récupère le bus à partir de l'instance précédente du fragment : http://developer.android.com/guide/topics/resources/runtime-changes.html
        mBus = inState.getParcelable(EVENT_BUS_STATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // A chaque fois que l'écran est en foreground on écoute les évènements, c'est un choix arbitraire, on pourrait très bien utiliser onStart() et onStop()
        mBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Dès que l'écran passe en background, on écoute plus les évènements
        mBus.unregister(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // L'instance courante du fragment va disparaître, par conséquence on sauvegarde le bus : http://developer.android.com/guide/topics/resources/runtime-changes.html
        outState.putParcelable(EVENT_BUS_STATE, mBus);
    }

    /**
     * Appelé quand MainFragment poste un évènement du type MainFragmentButtonEvent sur le bus qu'on lui a donné en paramètre
     *
     * @param event Evènement envoyé par MainFragment
     */
    @SuppressWarnings("unused")
    public void onEvent(MainFragment.MainFragmentButtonEvent event) {
        mTextView.setText("MainActivity : " + event.getValue());
    }
}
