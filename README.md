# EventBus Review #

Ce repo est un PoC d'une nouvelle manière de communiquer entre une Activity/Fragment parent et un fragment enfant.


## Mots employés ##

On appelle "parent" une Activity/Fragment qui instancie un enfant et qui s'occupe de l'afficher dans l'un de ses layouts.

On appelle "enfant" le fragment qui est affiché dans un parent.


## Rappels et état des lieux ##

Par convention un fragment est une objet modulable qui peut être instancié par n'importe quel parent. Pour qu'il puisse rester modulable, il est interdit que l'enfant fasse directement appel à une méthode du parent. Cette convention permet de limiter au maximum le couplage entre l'enfant et son parent.
Ainsi, pour que l'enfant puisse communiquer avec son parent, nous utilisons le système de BroadcastReceiver avec des conventions de nommage pour garder le code lisible et homogène. C'est à l'enfant de définir les String de l'ACTION et des EXTRA de l'intent. Quand il a besoin de transmettre une information à son parent, il créé un Intent avec l'ACTION correspondante et y ajoute les EXTRA puis broadcast l'intent avec la méthode sendBroadcast().

Quand au parent, il doit au préalable avoir enregistré un BroadcastReceiver avec l'ACTION de l'enfant. Ensuite, quand l'enfant transmet une information, le parent reçoit le Broadcast et traite l'information.

Ce système à 4 défauts :

* Performance mauvaise : Quand il existe des dizaines de BroadcastReceiver, la durée entre le sendBroadcast() et l'appel sur BroadcastReceiver peut atteindre quelques dixièmes de seconde.

* Broadcaster dans toute l'application : Le problème du Broadcast est qu'il broadcast justement, par principe, on n'envoie pas dans toute l'application une information qui n'est destinée qu'à son parent.

* Rendre tout Parcelable : Le moindre objet model de l'application doit forcement implémenter la classe Parcelable

* Convention de nommage strict : Pour ne pas mélanger toutes les String statiques en en-tête de classe, il faut mettre une convention de nommage pour les ACTION et les EXTRA.


## Le Bus d'évènement ##

Pour une définition, je vous invitie à chercher sur Google.
Dans notre cas, le bus d'évènement semble être une solution intéressante car qui permet de résoudre les 4 défauts précédemments cités.

Pour comprendre comment il est intégré dans notre problématique et comment l'instance du Bus est gérée, voici un [schéma](https://docs.google.com/drawings/d/1Kvsoe_thIVQcCKhSdVVoG4EGH_4dPYzV38qVzD6qEKs/edit?usp=sharing).

Pour notre PoC nous avons utilisé l'API [EventBus](https://github.com/greenrobot/EventBus) qui est plus rapide que son concurrent direct : [Otto](https://github.com/square/otto).


## Principe de base ##

Comme l'enfant ne dispose pas d'un accès direct à son parent, c'est alors au parent de lui fournir un moyen de communication. Ce moyen c'est une instance de EventBus. Quand le parent instancie son enfant, il lui fourni en paramètre une instance d'EventBus que l'enfant conserve précieusement. Quand l'enfant souhaite transmettre une information, alors il créé un évènement représenté par une instance de classe (la convention de nommage veut que cette classe se termine par le mot clef "Event" par exemple MyEvent) dans lequel l'enfant stocke la donnée qu'il veut transmettre. Une fois l'évènement créé, il le poste sur l'instance d'EventBus que son parent lui a fourni.

Pour que le parent puisse intercepter l'évènement il faut d'abord qui s'enregistre, il doit le faire en utilisant la même instance de d'EventBus qu'il partage avec l'enfant et avec la méthode register() et se désabonne avec la méthode unregister(). Ensuite, il suffit de créer une méthode publique nommée onEvent(MyEvent event). Quand l'enfant lève un évènement, c'est la méthode onEvent() qui est appelée automatiquement.

Il est possible de choisir dans quel thread nous souhaitons que l'évènement soit levé, pour en savoir plus c'est [pas là](https://github.com/greenrobot/EventBus#delivery-threads)

## Description de ce PoC ##

Ce PoC montre un exemple de 3 niveaux d'écrans avec :  MainActivity qui contient MainFragment qui contient MainChildFragment. Le MainActivity créé un bus qui est envoyé à MainFragment. MainFragment créé un bus qui est envoyé à MainChildFragment. De ce fait, MainFragment a 2 bus à gérer, celui sur lequel il répond à son parent, et celui auquel il s'abonne quand MainChildFragment répond.

MainChildFragment dispose d'un Button et d'un EditText, quand on clique sur "Throw event", MainChildFragment poste un évènement avec comme donnée la valeur de l'EditText. Ensuite MainFragment récupère l'évènement et l'affiche dans un TextView. C'est la même chose entre MainFragment et MainActivity.


## Problématiques rencontrées ##

Le problème des fragments, c'est que lorsque que l'on change l'orientation de l'écran, l'instance du Fragment est perdue et recréée de 0. Dans ce cas, nous perdons aussi les attributs d'instance et donc le bus d'évènement associé à l'instance. Pour palier ce problème, j'ai créé une surcharge de l'objet EventBus qui implémente l'interface Parcelable qui permet de sauver l'instance du Bus lors du "parceling" et de la récupérer dans le "deparceling". Cette instance est sauvée dans une map.